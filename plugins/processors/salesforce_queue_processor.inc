<?php
class salesforce_queue_processor extends salesforce_processor {
  /**
   * Process SalesForce over given handlers
   *
   * @param array $handlers
   *
   * @return unknown
   */
  public function processSalesforce() {
    $this->processSalesforceDeletes();
    $this->processSalesforceUpdates();

    return $this;
  }

  public function processDrupal($from = 0) {
    $sql = "SELECT sq.* FROM {salesforce_queue} sq INNER JOIN {salesforce_handlers} sh ON sh.sfhid = sq.sfhid ORDER BY sh.weight, sq.created";

    $result = (0 != $from) ? db_query_range($sql, $from, $this->getMaxDrupalProcess()) : db_query($sql);

    $operations = array();

    // Organize requests by operation then SF object
    while ($request = db_fetch_object($result)) {
      $transfer_success = TRUE;

      $request->data = unserialize($request->data);

      $handler = salesforce_queue_get_plugin($request->sfhid);

      if (FALSE != $handler) {
        $sfobj = $handler->getSalesforceObject();
        $external_id = $handler->getExternalId();

        if (FALSE === isset($operations[$request->operation])) {
          $operations[$request->operation] = array();
        }

        if (FALSE === isset($operations[$request->operation][$sfobj])) {
          $operations[$request->operation][$sfobj] = array();
        }

        if (FALSE === isset($operations[$request->operation][$sfobj][$external_id])) {
          $operations[$request->operation][$sfobj][$external_id] = array();
        }

        $function = 'processDrupal' . $request->operation;

        $operations[$request->operation][$sfobj][$external_id][$request->guid] = array(
          'handler' => $handler,
          'request' => $request,
        );
      }
    }

    foreach ($operations as $key => $objects) {
      switch ($key) {
        case 'upsert':
          $this->processDrupalUpserts($objects);
          break;

        case 'delete':
          $this->processDrupalDeletes($objects);
          break;
      }
    }

    return $this;
  }

  protected function processDrupalUpserts($objects) {
    $handlers = array();
    // Attempt to connect to Salesforce.
    $sf = salesforce_api_connect();
    if (!$sf) {
      drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
      return FALSE;
    }

    foreach ($objects as $sfobj => $items) {
      foreach ($items as $external_id => $requests) {
        // Check to see if there is a minimum number of upserts to process
        if (variable_get('salesforce_queue_minimum_type_upsert', 0) < sizeof($requests)) {
          $request_objects = array();
          $guids = array();

          foreach ($requests as $guid => $request) {
            $handler = $request['handler'];
            $request_item = $request['request'];
            $function = 'processDrupal' . $request_item->operation;

            $request_object = $handler->{$function}($request_item);

            // Get the current SFID so the handler can use the ID as the upsert check
            $sfdata = salesforce_api_id_load($request_item->drupal_type, $request_item->oid);

            if (FALSE === empty($sfdata->sfid) && FALSE === isset($request_object->Id)) {
              $request_object->Id = $sfdata->sfid;
            }

            if (FALSE != $request_object) {
              $request_objects[] = $request_object;
              $guids[] = $guid;
            }
            else {
              // If we fail to process correctly remove the queued request
              salesforce_queue_delete_queued_items(array($request_item->guid));
            }
          }

          while (0 < sizeof($request_objects)) {
            $request_success = TRUE;

            $current_requests = array_splice($request_objects, 0, variable_get('salesforce_queue_max_upsert', 200));
            $request_guids = array_splice($guids, 0, variable_get('salesforce_queue_max_upsert', 200));

            try {
              $results = $sf->client->upsert($external_id, $current_requests, $sfobj);

              // Make sure that the results is always an array
              $results = (TRUE === is_array($results)) ? $results : array($results);
            }
            catch (Exception $e) {
              $request_success = FALSE;
              salesforce_api_log(SALESFORCE_LOG_SOME, 'Exception while attempting to upsert nodes: ' . $e->getMessage(), array(), WATCHDOG_ERROR);
            }

            if (TRUE === $request_success) {
              for ($i = 0; $i < sizeof($results); $i++) {
                $result = $results[$i];
                if (TRUE == $result->success) {
                  $current_request = $requests[$request_guids[$i]]['request'];
                  salesforce_api_id_save($current_request->drupal_type, $current_request->oid, $result->id, 0);
                  salesforce_queue_add_transaction($current_request, $result->id, $sfobj);
                }
                else {
                  if (TRUE === isset($result->errors)) {
                    $errors = (TRUE === is_array($result->errors)) ? $result->errors : array($result->errors);

                    foreach ($errors as $error) {
                      $fields = (TRUE === is_array($error->fields)) ? implode(',', $error->fields) : $error->fields;

                      salesforce_api_log(SALESFORCE_LOG_SOME, 'Error when upserting ' . $sfobj . ' (' . $fields . '): ' . $error->statusCode . ': ' . $error->message, array(), WATCHDOG_ERROR);
                    }
                  }
                }
              }

              salesforce_queue_complete_queued_items($request_guids);
            }
          }
        }
      }
    }
  }

  protected function processDrupalDeletes($objects) {
    $handlers = array();

    // Attempt to connect to Salesforce.
    $sf = salesforce_api_connect();
    if (!$sf) {
      drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
      return FALSE;
    }

    foreach ($objects as $sfobj => $items) {
      foreach ($items as $external_id => $requests) {
        $ids = array();
        $guids = array();

        foreach ($requests as $guid => $request) {
          $sfid = $request['object'];
          $request_item = $request['request'];
          $sfid_info = salesforce_api_id_load($request_item->drupal_type, $request_item->oid);

          if (FALSE === empty($sfid_info->sfid)) {
            $ids[] = $sfid_info->sfid;
            $guids[] = $request_item->guid;

            // Splice all ids into chunks of a defined size
            while (0 < sizeof($ids)) {
              $request_success = TRUE;

              $current_request = array_splice($ids, 0, variable_get('salesforce_queue_max_delete', 200));
              $request_guids = array_splice($guids, 0, variable_get('salesforce_queue_max_delete', 200));

              try {
                $sf->client->delete($current_request);
              }
              catch (Exception $e) {
                $request_success = FALSE;
                salesforce_api_log(SALESFORCE_LOG_SOME, 'Exception while attempting to delete nodes: ' . $e->getMessage(), array(), WATCHDOG_ERROR);
              }

              if (TRUE === $request_success) {
                salesforce_queue_delete_object_map($current_request);
                salesforce_queue_complete_queued_items($request_guids);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Handle SalesForce deletions
   */
  protected function processSalesforceDeletes() {
    $deleted_records = array();

    foreach ($this->getSalesforceHandlers() as $key => $handlers) {
      if (FALSE === empty($handlers)) {
        $deletes = $this->getDeletedRecords($key);
        if (TRUE === is_array($deletes) && FALSE === empty($deletes)) {
          foreach ($handlers as $handler_definition) {
            $handler = salesforce_queue_get_plugin($handler_definition->sfhid);

            foreach ($deletes as $sfid) {
              if (NULL !== $sfid) {
                $object = new stdClass();
                $object->id = $sfid;

                $oid = $handler->processSalesforceDelete($object);

                if (FALSE != $oid) {
                  salesforce_queue_clear_queue_drupal_ids($handler->getDrupalType(), $oid, $handler->getHandlerId());
                  $deleted_records[] = $object->id;
                }
              }
            }
          }
        }
      }
    }

    salesforce_queue_delete_object_map($deleted_records);
  }


  /**
   * Handle SalesForce updates
   */
  protected function processSalesforceUpdates() {
    foreach ($this->getSalesforceHandlers() as $key => $handlers) {
      if (FALSE === empty($handlers)) {
        $updates = $this->getUpdatedRecords($key);

        foreach ($handlers as $handler_definition) {
          $handler = salesforce_queue_get_plugin($handler_definition->sfhid);

          foreach ($updates as $object) {
            $process_object = TRUE;

            if (FALSE === empty($object)) {

              // Check when the last transaction was
              $last_transaction = salesforce_queue_get_latest_transaction($key, $object->Id);

              if (FALSE != $last_transaction) {
                // If we got a transaction check the modification dates to see if we should process it
                $object_update = (TRUE === isset($object->LastModifiedDate)) ? strtotime($object->LastModifiedDate) : 0;
                $last_processed = strtotime($last_transaction->processed);

                if ($object_update <= $last_processed) {
                  $process_object = FALSE;
                }
              }

              if (TRUE === $process_object) {
                try {
                  $oid = $handler->processSalesforceUpdate($object);
                  $this->clearQueuedRecords($key, array($object->Id), 'update');

                  if (FALSE != $oid) {
                    salesforce_api_id_save($handler->getDrupalType(), $oid, $object->Id, 0);
                  }
                }
                catch (Exception $e) {
                  salesforce_api_log(SALESFORCE_LOG_SOME, 'Exception while attempting to get updated records: ' . $e->getMessage(), array(), WATCHDOG_ERROR);
                }
              }
              else {
                $this->clearQueuedRecords($key, array($object->Id), 'update');
              }
            }
          }
        }

        // If we process the max number exit to avoid problems with dependencies
        if (sizeof($updates) == $this->getMaxSalesforceProcess()) {
          break;
        }
      }
    }
  }
}
