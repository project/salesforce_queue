<?php
class salesforce_salesforce_scheduler extends salesforce_scheduler {
  const SALESFORCE_MIN_TIME = '-29 days';

  /**
   * Sets the last run attributes
   *
   * @param int $last_run
   *
   * @return salesforce_scheduler
   */
  public function setLastRun($last_run) {
    $last_run = (int)$last_run;

    // Salesforce allows getUpdated to be checked as far back as 30 days
    $min_time = strtotime(self::SALESFORCE_MIN_TIME);
    $last_run = ($last_run >= $min_time) ? $last_run : $min_time;

    // There's a minimum time period of 60 seconds between start and end time
    if ((time() - $last_run) <= 60) {
      $last_run = time() - 61;
    }

    $this->_last_run = (int)$last_run;
    return $this;
  }

  public function scheduleUpserts() {
    foreach ($this->getHandlers() as $handler) {
      if ('sync' == $handler->getOperation()) {
        $this->queueUpdatedRecords($handler->getSalesforceObject());
      }
    }
  }

  public function scheduleDeletes() {
    foreach ($this->getHandlers() as $handler) {
      if ('sync' == $handler->getOperation()) {
        $this->queueDeletedRecords($handler->getSalesforceObject());
      }
    }
  }

  public function queueDeletedRecords($sfobj) {
    // Attempt to connect to Salesforce.
    $sf = salesforce_api_connect();
    if (!$sf) {
      drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
      return FALSE;
    }

    $now = time();

    $updates = array();

    try {
      $deleted_items = $sf->client->getDeleted($sfobj, $this->getLastRun(), $now);
      if (TRUE === isset($deleted_items->deletedRecords)) {
        $deleted_records = (TRUE === is_array($deleted_items->deletedRecords)) ? $deleted_items->deletedRecords : array($deleted_items->deletedRecords);

        foreach ($deleted_records as $deletion) {
          db_query("DELETE FROM {salesforce_queue_sf} WHERE sfobj = '%s' AND sfid = '%s' AND operation = '%s'", $sfobj, $deletion->id, 'delete');
          db_query("INSERT INTO {salesforce_queue_sf} (sfobj, sfid, operation) VALUES ('%s', '%s', '%s')", $sfobj, $deletion->id, 'delete');
        }
      }

      if (TRUE === isset($deleted_items->latestDateCovered)) {
        // Set the last processed date to the SalesForce latest date covered just in case we get a subset of deletes
        $time = strtotime($deleted_items->latestDateCovered);
        if ($time < $this->getLastRun()) {
          variable_set('salesforce_queue_last_processed', $time);
        }
      }
    }
    catch (Exception $e) {
      $request_successful = FALSE;
      salesforce_api_log(SALESFORCE_LOG_SOME, 'Exception while attempting to get deleted records: ' . $e->getMessage(), array(), WATCHDOG_ERROR);
    }

    return $this;
  }

  public function queueUpdatedRecords($sfobj) {
    // Attempt to connect to Salesforce.
    $sf = salesforce_api_connect();
    if (!$sf) {
      drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
      return FALSE;
    }

    $now = time();

    $updates = array();

    $updated_items = $sf->client->getUpdated($sfobj, $this->getLastRun(), $now);

    if (TRUE === isset($updated_items->latestDateCovered)) {
      // Set the last processed date to the SalesForce latest date covered just in case we get a subset of updates
      $time = strtotime($updated_items->latestDateCovered);
      if ($time < $this->getLastRun()) {
        variable_set('salesforce_queue_last_processed', $time);
      }
    }

    // Make sure the return is an array
    if (TRUE === isset($updated_items->ids)) {
      $updates = (TRUE === is_array($updated_items->ids)) ? $updated_items->ids : array($updated_items->ids);
      foreach ($updates as $sfid) {
        db_query("DELETE FROM {salesforce_queue_sf} WHERE sfobj = '%s' AND sfid = '%s' AND operation = '%s'", $sfobj, $sfid, 'update');
        db_query("INSERT INTO {salesforce_queue_sf} (sfobj, sfid, operation) VALUES ('%s', '%s', '%s')", $sfobj, $sfid, 'update');
      }
    }

    return $this;
  }
}