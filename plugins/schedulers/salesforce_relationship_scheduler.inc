<?php
class salesforce_relationship_scheduler extends salesforce_scheduler {
  /**
   * Schedule relationships for upserts
   */
  protected function scheduleUpserts() {
    $args = array();

    foreach ($this->getHandlers() as $handler) {
      if ('upsert' == $handler->getOperation()) {
        if ($handler instanceof salesforce_relationship_handler) {
          $args[] = $handler->getRelationshipType();
        }
      }
    }

    if (0 < sizeof($args)) {
      $relationships = user_relationships_load(array(
        'rtid' => $args,
        'created_at' => '> ' . $this->getLastRun()
      ));

      foreach ($relationships as $relationship) {
        foreach (salesforce_queue_get_drupal_object_handler('upsert', $relationship) as $handler) {
          if ($handler instanceof salesforce_handler_upsert) {
            $handler->scheduleDrupalUpsert($relationship);
          }
        }
      }
    }
  }

  /**
   * Schedule relationships for deletion
   */
  protected function scheduleDeletes() {
    $node_types = array();
    $relationship_types = array();

    foreach ($this->getHandlers() as $handler) {
      if ('delete' == $handler->getOperation()) {
        if ($handler instanceof salesforce_relationship_handler) {
          $relationship_types[] = $handler->getDrupalType();
        }
      }
    }

    if (0 < sizeof($relationship_types)) {
      $placeholders = implode(',', array_fill(0, count($relationship_types), "'%s'"));

      $result = db_query("SELECT r.rid, r.rtid, som.drupal_type AS type, som.* FROM {salesforce_object_map} som
      	LEFT OUTER JOIN {user_relationships} r ON r.rid = som.oid
      	WHERE r.rid IS NULL AND som.drupal_type IN ($placeholders)", $relationship_types);

      while ($delete_info = db_fetch_object($result)) {
        foreach (salesforce_queue_get_drupal_object_handler('delete', $delete_info) as $handler) {
          if ($handler instanceof salesforce_handler_delete) {
            $handler->scheduleDrupalDelete($delete_info);
          }
        }
      }
    }
  }
}