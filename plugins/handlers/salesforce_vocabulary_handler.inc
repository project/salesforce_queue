<?php
class salesforce_vocabulary_handler extends salesforce_handler {
  protected $_vocabulary;

  public function optionsForm() {
    $form = parent::optionsForm();

    $options = array();
    foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
      $options[$vid] = $vocabulary->name;
    }

    $form['vid'] = array(
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#options' => count($options) > 0 ? $options : array(t('None available')),
      '#disabled' => count($options) == 0,
      '#required' => FALSE,
      '#default_value' => $this->getVid(),
    );

    return $form;
  }

  /**
   * Return whether this handler can work with the particular node type
   *
   * @return boolean
   */
  public function isHandlerFor($object) {
    if (TRUE === isset($object->type)) {
      $node_vocabularies = taxonomy_get_vocabularies($object->type);

      return (TRUE === isset($node_vocabularies[$this->getVid()]));
    }

    return FALSE;
  }

  public function setVid($node_type) {
    $this->_settings['vid'] = $node_type;
    return $this;
  }

  public function getVid() {
    if (TRUE === isset($this->_settings['vid'])) {
      return $this->_settings['vid'];
    }

    return '';
  }

  /**
   * Returns the vocabulary associated with the handler
   *
   * @return stdClass
   */
  public function getVocabulary() {
    $vid = $this->getVid();

    return (FALSE === empty($vid)) ? taxonomy_vocabulary_load($vid) : NULL;
  }

  public function getDrupalType() {
    $vocabulary = $this->getVocabulary();

    return (NULL !== $vocabulary) ? $vocabulary->name : '';
  }
}