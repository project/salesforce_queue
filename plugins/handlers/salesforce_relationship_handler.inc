<?php
class salesforce_relationship_handler extends salesforce_handler {
  public function optionsForm() {
    $form = parent::optionsForm();

    $relationship_types = user_relationships_types_load();

    $options = array();

    foreach (user_relationships_types_load() as $relationship_type) {
      $options[$relationship_type->rtid] = $relationship_type->name;
    }

    $form['relationship_type'] = array(
      '#type' => 'select',
      '#title' => t('Relationship Type'),
      '#options' => count($options) > 0 ? $options : array(t('None available')),
      '#disabled' => count($options) == 0,
      '#required' => FALSE,
      '#default_value' => $this->getRelationshipType(),
    );

    return $form;
  }

  /**
   * Return whether this handler can work with the particular relationship type
   *
   * @return boolean
   */
  public function isHandlerFor($object) {
    return (TRUE === isset($object->rtid) && $this->getRelationshipType() == $object->rtid) || (TRUE === isset($object->drupal_type) && $this->getDrupalType() == $object->drupal_type);
  }

  public function setRelationshipType($rtid) {
    $this->_settings['relationship_type'] = $rtid;
    return $this;
  }

  public function getRelationshipType() {
    if (TRUE === isset($this->_settings['relationship_type'])) {
      return $this->_settings['relationship_type'];
    }

    return '';
  }

  public function getDrupalType() {
    $relationship_type = user_relationships_type_load($this->getRelationshipType());

    return $relationship_type->name;
  }
}