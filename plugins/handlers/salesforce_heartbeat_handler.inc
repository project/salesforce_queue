<?php
class salesforce_heartbeat_handler extends salesforce_handler {
  public function optionsForm() {
    $form = parent::optionsForm();

    $options = array();

    foreach (heartbeat_messages() as $message) {
      $options[$message->message_id] = $message->description;
    }

    $form['message_id'] = array(
      '#type' => 'select',
      '#title' => t('Message Type'),
      '#options' => count($options) > 0 ? $options : array(t('None available')),
      '#disabled' => count($options) == 0,
      '#required' => FALSE,
      '#default_value' => $this->getMessageId(),
    );

    $form['anonymous_scheduled'] = array(
      '#type' => 'select',
      '#title' => t('Schedule Anonymous'),
      '#options' => array(
        0 => t('No'),
        1 => t('Yes')
      ),
      '#disabled' => count($options) == 0,
      '#required' => FALSE,
      '#default_value' => $this->isAnonymousScheduled(),
    );

    $form['admin_scheduled'] = array(
      '#type' => 'select',
      '#title' => t('Schedule Admin'),
      '#options' => array(
        0 => t('No'),
        1 => t('Yes')
      ),
      '#disabled' => count($options) == 0,
      '#required' => FALSE,
      '#default_value' => $this->isAdminScheduled(),
    );

    return $form;
  }

  /**
   * Return whether this handler can work with the particular relationship type
   *
   * @return boolean
   */
  public function isHandlerFor($object) {
    return (TRUE === isset($object->drupal_type) && $this->getDrupalType() == $object->drupal_type) || (TRUE === isset($object->message_id) && $this->getDrupalType() == $object->message_id);
  }

  /**
   * Sets the heartbeat message id
   *
   * @param string $message_id
   *
   * @return salesforce_heartbeat_handler
   */
  public function setMessageId($message_id) {
    $this->_settings['message_id'] = $message_id;
    return $this;
  }

  /**
   * Returns the heartbeat message id
   *
   * @return string
   */
  public function getMessageId() {
    if (TRUE === isset($this->_settings['message_id'])) {
      return $this->_settings['message_id'];
    }

    return '';
  }

  /**
   * Returns whether to schedule anonymous heartbeats
   *
   * @return string
   */
  public function isAnonymousScheduled() {
    if (TRUE === isset($this->_settings['anonymous_scheduled'])) {
      return (boolean)$this->_settings['anonymous_scheduled'];
    }

    return FALSE;
  }

  /**
   * Returns whether to schedule admin heartbeats
   *
   * @return string
   */
  public function isAdminScheduled() {
    if (TRUE === isset($this->_settings['admin_scheduled'])) {
      return (boolean)$this->_settings['admin_scheduled'];
    }

    return FALSE;
  }

  /**
   * Returns the hearbeat message
   *
   * @return stdClass
   */
  public function getMessage() {
    return heartbeat_message_load($this->getMessageId());
  }

  public function getDrupalType() {
    return $this->getMessageId();
  }
}