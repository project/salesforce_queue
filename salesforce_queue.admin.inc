<?php
function salesforce_queue_handler_admin() {
  // Define the header for the admin table.
  $header = array(
    t('Machine Name'),
    t('Description'),
    t('SalesForce object'),
    t('Operation'),
    t('Handler'),
    array(
      'data' => t('Operations'),
      'colspan' => 3
    )
  );
  $rows = array();
  $handler_registry = salesforce_queue_handlers();

  foreach (salesforce_queue_load_handlers(TRUE) as $handler) {
    if (TRUE == isset($handler_registry[$handler->plugin])) {
      $handler_info = $handler_registry[$handler->plugin];
      $rows[] = array(
        $handler->machine_name,
        $handler->description,
        $handler->sfobj,
        $handler->operation,
        $handler_info['title'],
        l(t('edit'), SALESFORCE_PATH_ADMIN . '/handlers/' . $handler->sfhid . '/edit'),
        ((1 == $handler->status) ? l(t('disable'), SALESFORCE_PATH_ADMIN . '/handlers/' . $handler->sfhid . '/disable') : l(t('enable'), SALESFORCE_PATH_ADMIN . '/handlers/' . $handler->sfhid . '/enable')),
        l(t('delete'), SALESFORCE_PATH_ADMIN . '/handlers/' . $handler->sfhid . '/delete'),
        l(t('queue'), SALESFORCE_PATH_ADMIN . '/handlers/' . $handler->sfhid . '/queue'),
      );
    }
  }

  // Add a message if no objects have been mapped.
  if (count($rows) == 0) {
    $rows[] = array(
      array(
        'data' => t('You have not yet defined any fieldmaps.'),
        'colspan' => 6
      ),
    );
  }

  return theme('table', $header, $rows);
}

/**
 * Menu handler to enable / disable a handler
 *
 * @param int $sfhid
 * @param int $status
 */
function salesforce_queue_handler_enable_disable($sfhid, $status) {
  $map = array(
    'sfhid' => $sfhid,
    'status' => $status,
  );

  salesforce_queue_handler_save($map);

  drupal_goto(SALESFORCE_PATH_ADMIN . '/handlers');
}

/**
 * Displays the form to add a handler.
 */
function salesforce_queue_handler_add_form(&$form_state) {
  $form = array();

  // Build an options array out of the Drupal objects.
  $options = array();

  foreach (salesforce_queue_handlers() as $handler) {
    $options[$handler['plugin']] = $handler['title'];
  }
  asort($options);
  $form['plugin'] = array(
    '#type' => 'select',
    '#title' => t('Handler'),
    '#options' => count($options) > 0 ? $options : array(t('None available')),
    '#disabled' => count($options) == 0,
    '#required' => TRUE,
  );

  $form['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#options' => array(
      'upsert' => 'Upsert',
      'delete' => 'Delete',
      'sync' => 'Sync'
    ),
    '#required' => TRUE,
  );

  // Build an options array out of the Salesforce objects.
  $options = array();

  foreach (salesforce_api_fieldmap_objects_load('salesforce') as $key => $value) {
    $options[$key] = $value['label'];
  }
  asort($options);

  $form['machine_name'] = array(
    '#type' => 'textbox',
    '#title' => t('Machine Name'),
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textbox',
    '#title' => t('Description'),
    '#required' => FALSE,
  );

  $form['sfobj'] = array(
    '#type' => 'select',
    '#title' => t('Salesforce object'),
    '#options' => count($options) > 0 ? $options : array(t('None available')),
    '#disabled' => count($options) == 0,
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Handler'),
    '#suffix' => l(t('Cancel'), SALESFORCE_PATH_ADMIN . '/handlers'),
  );

  return $form;
}

function salesforce_queue_handler_add_form_submit($form, &$form_state) {
  $index = salesforce_queue_handler_create($form_state['values']);

  // Redirect to its edit form.
  $form_state['redirect'] = SALESFORCE_PATH_ADMIN . '/handlers/' . $index . '/edit';
}

function salesforce_queue_handler_edit($form_state, $sfhid) {
  $form = array(
    'sfhid' => array(
      '#type' => 'value',
      '#value' => $sfhid,
    ),
  );

  $plugin = salesforce_queue_get_plugin($sfhid);

  // Build an options array out of the Salesforce objects.
  $options = array();

  foreach (salesforce_api_fieldmap_objects_load('salesforce') as $key => $value) {
    $options[$key] = $value['label'];
  }
  asort($options);

  $form['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine Name'),
    '#default_value' => $plugin->getMachineName(),
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $plugin->getDescription(),
    '#required' => FALSE,
  );

  $form['sfobj'] = array(
    '#type' => 'select',
    '#title' => t('Salesforce object'),
    '#options' => count($options) > 0 ? $options : array(t('None available')),
    '#disabled' => count($options) == 0,
    '#default_value' => $plugin->getSalesforceObject(),
    '#required' => FALSE,
  );

  if (FALSE !== $plugin) {
    $form = $form + $plugin->optionsForm();
  }

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $plugin->getWeight(),
    '#delta' => 20,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

function salesforce_queue_handler_edit_submit($form, &$form_state) {
  $sfhid = $form_state['values']['sfhid'];
  $plugin = salesforce_queue_get_plugin($sfhid);

  $values = $form_state['values'];

  unset($values['sfobj']);
  unset($values['weight']);
  unset($values['drupal_type']);

  $map = array(
    'sfhid' => $sfhid,
    'settings' => $values,
    'sfobj' => $form_state['values']['sfobj'],
    'weight' => $form_state['values']['weight'],
    'drupal_type' => $form_state['values']['drupal_type'],
    'machine_name' => $form_state['values']['machine_name'],
    'description' => $form_state['values']['description'],
  );

  salesforce_queue_handler_save($map);

  $form_state['redirect'] = SALESFORCE_PATH_ADMIN . '/handlers';
}

/**
 * Confirm form to delete a handler
 *
 * @param array $form_state
 * @param int $handler
 */
function salesforce_queue_handler_delete_confirm(&$form_state, $handler) {
  $redirect = SALESFORCE_PATH_ADMIN . '/handlers';

  // Add the fieldmap to the form array.
  $form['handler'] = array(
    '#type' => 'value',
    '#value' => (int)$handler,
  );

  return confirm_form($form, t('Are you sure you want to delete this handler?'), $redirect, $desc, t('Delete'));
}

/**
 * Submit handler for deletion
 *
 * @param array $form
 * @param array $form_state
 */
function salesforce_queue_handler_delete_confirm_submit($form, &$form_state) {
  // Delete the specified handler.
  salesforce_queue_handler_delete($form_state['values']['handler']);

  // Display a message and return to the admin screen.
  drupal_set_message(t('The handler has been deleted.'));

  $form_state['redirect'] = SALESFORCE_PATH_ADMIN . '/handlers';
}

function salesforce_queue_queue_admin() {
  return 'test';
}

/**
 * Triggers scheduling / queuing on a particular handler
 *
 * @param int $sfhid
 */
function salesforce_queue_handler_schedule_handler($sfhid) {
  $handler = salesforce_queue_get_plugin($sfhid);
  _salesforce_queue_handler_schedule($handler);
  drupal_set_message('Queued Handler: ' . $handler->getMachineName());
  drupal_goto(SALESFORCE_PATH_ADMIN . '/handlers/list');
}

/**
 * Schedules a SalesforceHandler
 *
 * @param salesforce_handler_sync $handler
 */
function _salesforce_queue_handler_schedule($handler) {
  $plugins = ctools_get_plugins('salesforce', 'plugins');

  foreach (salesforce_queue_schedulers() as $scheduler_info) {
    $class = ctools_plugin_get_class($plugins[$scheduler_info['plugin']], 'handler');
    $scheduler = new $class();
    $scheduler->setLastRun(0)->setHandlers(array($handler))->schedule(TRUE);
  }
}
