<?php
class UUID {
  protected $_uuidobject;

  protected $_urand;

  protected static $_instance;

  /**
   * Default constructor
   *
   * @return Forumone_Util_UUID
   */
  protected function __construct() {
    $this->_urand = @fopen('/dev/urandom', 'rb');
  }

  /**
   * Returns an instance of the UUID class
   *
   * @static
   * @return Forumone_Util_UUID
   */
  public static function getInstance() {
    if (null === self::$_instance) {
      self::$_instance = new UUID();
    }

    return self::$_instance;
  }

  /**
   * Returns a UUID
   *
   * @static
   * @return unknown_type
   */
  public static function get() {
    return self::getInstance()->getUUID();
  }

  /**
   * Returns a UUID
   *
   * @return string
   */
  public function getUUID() {
    if (true === function_exists('uuid_create') && true === function_exists('uuid_make')) {
      return $this->v4();
    }
    else {
      return $this->getRandom();
    }
  }

  /**
   * Generates a UUID from /dev/urandom or mt_rand as a fallback if the UUID PECL library is not installed
   *
   * @return string
   */
  protected function getRandom() {
    $pr_bits = false;
    if (is_resource($this->_urand)) {
      $pr_bits .= @fread($this->_urand, 16);
    }
    if (!$pr_bits) {
      $fp = @fopen('/dev/urandom', 'rb');
      if ($fp !== false) {
        $pr_bits .= @fread($fp, 16);
        @fclose($fp);
      }
      else {
        // If /dev/urandom isn't available (eg: in non-unix systems), use mt_rand().
        $pr_bits = "";
        for ($cnt = 0; $cnt < 16; $cnt++) {
          $pr_bits .= chr(mt_rand(0, 255));
        }
      }
    }
    $time_low = bin2hex(substr($pr_bits, 0, 4));
    $time_mid = bin2hex(substr($pr_bits, 4, 2));
    $time_hi_and_version = bin2hex(substr($pr_bits, 6, 2));
    $clock_seq_hi_and_reserved = bin2hex(substr($pr_bits, 8, 2));
    $node = bin2hex(substr($pr_bits, 10, 6));

    /**
     * Set the four most significant bits (bits 12 through 15) of the
     * time_hi_and_version field to the 4-bit version number from
     * Section 4.1.3.
     * @see http://tools.ietf.org/html/rfc4122#section-4.1.3
     */
    $time_hi_and_version = hexdec($time_hi_and_version);
    $time_hi_and_version = $time_hi_and_version >> 4;
    $time_hi_and_version = $time_hi_and_version | 0x4000;

    /**
     * Set the two most significant bits (bits 6 and 7) of the
     * clock_seq_hi_and_reserved to zero and one, respectively.
     */
    $clock_seq_hi_and_reserved = hexdec($clock_seq_hi_and_reserved);
    $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved >> 2;
    $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved | 0x8000;

    return sprintf('%08s-%04s-%04x-%04x-%012s', $time_low, $time_mid, $time_hi_and_version, $clock_seq_hi_and_reserved, $node);
  }

  /**
   * On long running deamons i've seen a lost resource. This checks the resource and creates it if needed.
   *
   */
  protected function create() {
    if (!is_resource($this->uuidobject)) {
      uuid_create($this->uuidobject);
    }
  }

  /**
   * Return a type 1 (MAC address and time based) uuid
   *
   * @return string
   */
  protected function v1() {
    $this->create();
    uuid_make($this->uuidobject, UUID_MAKE_V1);
    uuid_export($this->uuidobject, UUID_FMT_STR, $uuidstring);
    return trim($uuidstring);
  }

  /**
   * Return a type 4 (random) uuid
   *
   * @return String
   */
  protected function v4() {
    $this->create();
    uuid_make($this->uuidobject, UUID_MAKE_V4);
    uuid_export($this->uuidobject, UUID_FMT_STR, $uuidstring);
    return trim($uuidstring);
  }

  /**
   * Return a type 5 (SHA-1 hash) uuid
   *
   * @return String
   */
  protected function v5() {
    $this->create();
    uuid_make($this->uuidobject, UUID_MAKE_V5);
    uuid_export($this->uuidobject, UUID_FMT_STR, $uuidstring);
    return trim($uuidstring);
  }
}