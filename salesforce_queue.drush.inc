<?php
/**
 * @file
 *   drush integration for salesforce queue module.
 */

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * @see drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function salesforce_queue_drush_command() {
  $items = array();

  // the key in the $items array is the name of the command.
  $items['salesforce-queue-execute'] = array( // the name of the function implementing your command.
    'callback' => 'salesforce_queue_drush_run_queue',
    // a short description of your command
    'description' => dt('Runs the salesforce queue'),
  );

  $items['sq-schedule'] = array(
    'callback' => 'salesforce_queue_drush_schedule',
    'description' => dt('Schedules a SalesForce handler'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function salesforce_queue_drush_help($section) {
  switch ($section) {
    case 'drush:salesforce-queue-execute':
      return dt("Runs all enabled schedulers and processors to synchronize with SalesForce");
  }
}

/**
 * Example drush command callback.
 *
 * This is where the action takes place.
 *
 * In this function, all of Drupals API is (usually) available, including
 * any functions you have added in your own modules/themes.
 *
 * To print something to the terminal window, use drush_print().
 *
 */
function salesforce_queue_drush_run_queue() {
  salesforce_queue_run_queue();
}

/**
 * Drush command to queue a SalesForce handler
 *
 * @param int $sfhid
 */
function salesforce_queue_drush_schedule($sfhid) {
  module_load_include('inc', 'salesforce_queue', 'salesforce_queue.admin');

  $handler = salesforce_queue_get_plugin($sfhid);

  if (FALSE != $handler && $handler instanceof salesforce_handler_sync) {
    _salesforce_queue_handler_schedule($handler);
  }
}
