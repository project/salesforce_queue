<?php
function salesforce_queue_get_queue($count = 0, $from = 0) {
  $sql = "SELECT * FROM {salesforce_queue} ORDER BY created";

  $result = (0 != $count) ? db_query_range($sql, $count, $from) : db_query($sql);

  $queue = array();

  while ($transfer = db_fetch_object($result)) {
    $queue[] = $transfer;
  }

  return $queue;
}

function salesforce_queue_process_updates() {
  $queue = salesforce_queue_get_queue(variable_get('salesforce_queue_max_process', 1000));

  $operations = array();

  // Organize requests by operation and SalesForce object type
  foreach ($queue as $item) {
    $transfer_success = TRUE;

    if (FALSE === isset($operations[$item->operation])) {
      $operations[$item->operation] = array();
    }

    if (FALSE === isset($operations[$item->operation][$item->sfid])) {
      $operations[$item->operation][$item->sfid] = array();
    }

    $operations[$item->operation][$item->sfid] = $item->data;
  }

  foreach ($operations as $operation => $records) {
    module_invoke_all('salesforce_operation_' . $operation, $records);
  }
}

/**
 * Deletes records in SalesForce
 *
 * @param array $records
 */
function salesforce_queue_salesforce_operation_delete($records) {
  // Attempt to connect to Salesforce.
  $sf = salesforce_api_connect();
  if (!$sf) {
    drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
    return FALSE;
  }

  $ids = array();
  $guids = array();

  /**
   * Expects each row in records to be an associative array keyed by the
   * oid with the value an array of objects with a field for SFID and one for
   * the request GUID
   */
  foreach ($records as $oid => $items) {
    foreach ($items as $item) {
      if (TRUE === isset($item->sfid)) {
        $ids[] = $item->sfid;
        $guids[] = $item->guid;
      }
    }
  }

  // Splice all ids into chunks of a defined size
  while (0 < sizeof($ids)) {
    $request_success = TRUE;

    $current_request = array_splice($ids, 0, variable_get('salesforce_queue_max_delete', 200));
    $request_guids = array_splice($guids, 0, variable_get('salesforce_queue_max_delete', 200));

    try {
      $sf->client->delete($current_request);
    }
    catch (Exception $e) {
      $request_success = FALSE;
      salesforce_api_log(SALESFORCE_LOG_SOME, 'Exception while attempting to delete nodes: ' . $e->getMessage(), array(), WATCHDOG_ERROR);
    }

    if (TRUE === $request_success) {
      salesforce_queue_delete_object_map($current_request);
      salesforce_queue_delete_queued_items($request_guids);
    }
  }
}

function salesforce_queue_salesforce_operation_upsert($records) {
  // Attempt to connect to Salesforce.
  $sf = salesforce_api_connect();
  if (!$sf) {
    drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
    return FALSE;
  }

  foreach ($records as $sfid => $items) {
    $handlers = salesforce_queue_get_salesforce_object_handlers('upsert', $sfid);

    // Check to see if there is a minimum number of upserts to process
    if (FALSE === empty($handlers) && variable_get('salesforce_queue_minimum_type_upsert', 0) < sizeof($items)) {
      $values = array();
      $guids = array();

      foreach ($items as $item) {
        $values[] = $item->data;
        $guids[] = $item->guid;
      }

      while (0 < sizeof($values)) {
        $request_success = TRUE;

        $current_request = array_splice($values, 0, variable_get('salesforce_queue_max_upsert', 200));
        $request_guids = array_splice($guids, 0, variable_get('salesforce_queue_max_upsert', 200));

        foreach ($handlers as $handler) {
          try {
            $sf->client->upsert($handler->getExternalId(), $current_request, $sfid);
          }
          catch (Exception $e) {
            $request_success = FALSE;
            salesforce_api_log(SALESFORCE_LOG_SOME, 'Exception while attempting to upsert nodes: ' . $e->getMessage(), array(), WATCHDOG_ERROR);
          }
        }

        if (TRUE === $request_success) {
          salesforce_queue_delete_object_map($current_request);
          salesforce_queue_delete_queued_items($request_guids);
        }
      }
    }
  }
}

function salesforce_queue_get_deleted($sfid, $time) {
  // Attempt to connect to Salesforce.
  $sf = salesforce_api_connect();

  if (!$sf) {
    drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
    return FALSE;
  }

  $now = time();

  $deleted_items = $sf->client->getDeleted($sfid, $time, $now);

  return $deleted_items->deletedRecords;
}

function salesforce_queue_get_updated($sfid, $time) {
  // Attempt to connect to Salesforce.
  $sf = salesforce_api_connect();
  if (!$sf) {
    drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
    return FALSE;
  }

  $now = time();

  $updates = array();

  $updated_items = $sf->client->getUpdated($sfid, $time, $now);

  $results = array();

  if (0 < sizeof($updated_items->ids)) {
    $updates = $updated_items->ids;

    $objects = salesforce_api_fieldmap_objects_load('salesforce', $sfid);

    $fields = implode(',', array_keys($objects['fields']));

    while (0 < sizeof($updates)) {
      $request = array_splice($updates, 0, SALESFORCE_QUEUE_MAX_RETRIEVE);
      $request_result = $sf->client->retrieve($fields, $sfid, $request);
      $results = array_merge($results, $request_result);
    }
  }

  return $results;
}