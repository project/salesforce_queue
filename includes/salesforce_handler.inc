<?php
abstract class salesforce_handler {
  protected $_settings = array();
  protected $_sfobj;
  protected $_weight;
  protected $_machine_name;
  protected $_description;
  protected $_handler_id;
  protected $_operation;

  public function setHandlerId($handler_id) {
    $this->_handler_id = (int)$handler_id;
    return $this;
  }

  public function getHandlerId() {
    return $this->_handler_id;
  }

  public function setSalesforceObject($sfobj) {
    $this->_sfobj = (string)$sfobj;
    return $this;
  }

  public function getSalesforceObject() {
    return $this->_sfobj;
  }

  public function getMachineName() {
    return $this->_machine_name;
  }

  public function setMachineName($machine_name) {
    $this->_machine_name = (string)$machine_name;
    return $this;
  }

  public function getOperation() {
    return $this->_operation;
  }

  public function setOperation($operation) {
    $this->_operation = (string)$operation;
    return $this;
  }

  public function getDescription() {
    return $this->_description;
  }

  public function setDescription($description) {
    $this->_description = (string)$description;
    return $this;
  }

  public function setWeight($weight) {
    $this->_weight = (int)$weight;
    return $this;
  }

  public function getWeight() {
    return $this->_weight;
  }

  public function setSettings($settings) {
    $this->_settings = $settings;
    return $this;
  }

  public function getSettings() {
    return $this->_settings;
  }

  public function getExternalId() {
    if (TRUE === isset($this->_settings['external_id'])) {
      return $this->_settings['external_id'];
    }

    return '';
  }

  public function optionsForm() {
    $form = array();

    $options = array();

    $targets = salesforce_api_fieldmap_objects_load('salesforce', $this->_sfobj);

    if (FALSE === empty($targets['fields'])) {
      foreach ($targets['fields'] as $field_name => $field) {
        $options[$field_name] = $field['label'] . ' (' . $field_name . ')';
      }
    }

    $form['external_id'] = array(
      '#type' => 'select',
      '#title' => t('External ID Field'),
      '#description' => 'Field that is used to verify an upsert',
      '#required' => TRUE,
      '#default_value' => (TRUE === isset($this->_settings['external_id'])) ? $this->_settings['external_id'] : NULL,
      '#options' => $options,
    );

    return $form;
  }

  public function getLatestTransactionDate($oid) {
    $transaction = salesforce_queue_load_transaction($this->getDrupalType(), $oid);

    return (FALSE !== $transaction) ? strtotime($transaction->processed) : 0;
  }

  abstract public function getDrupalType();

  abstract public function isHandlerFor($object);
}

interface salesforce_handler_upsert {
  public function processDrupalUpsert($request);

  public function scheduleDrupalUpsert($object);
}

interface salesforce_handler_sync {
  public function processSalesforceUpdate($object);

  public function processSalesforceDelete($object);
}

interface salesforce_handler_delete {
  public function processDrupalDelete($request);

  public function scheduleDrupalDelete($object);
}
