<?php
abstract class salesforce_scheduler {
  /**
   * Timestamp of last run
   * @var int
   */
  protected $_last_run;

  /**
   * Handlers
   * @var array
   */
  protected $_handlers;

  /**
   * Schedule items
   *
   * @param boolean $force
   * Whether the scheduler should schedule all items
   *
   * @return salesforce_scheduler
   */
  public function schedule($force = FALSE) {
    $this->scheduleDeletes();
    $this->scheduleUpserts();

    return $this;
  }

  /**
   * Sets the last run attributes
   *
   * @param int $last_run
   *
   * @return salesforce_scheduler
   */
  public function setLastRun($last_run) {
    $this->_last_run = (int)$last_run;
    return $this;
  }

  /**
   * Returns the last run timestamp
   *
   * @return int
   */
  public function getLastRun() {
    return $this->_last_run;
  }

  /**
   * Sets the handlers to schedule
   *
   * @param array $handlers
   *
   * @return salesforce_scheduler
   */
  public function setHandlers($handlers) {
    if (TRUE === is_array($handlers)) {
      $this->_handlers = array();

      foreach ($handlers as $handler) {
        $handler = ($handler instanceof salesforce_handler) ? $handler : salesforce_queue_get_plugin($handler->sfhid);
        if (TRUE === is_object($handler)) {
          $this->_handlers[] = $handler;
        }
      }
    }

    return $this;
  }

  public function getHandlers() {
    if (NULL === $this->_handlers) {
      $this->setHandlers(salesforce_queue_load_handlers());
    }

    return (TRUE === is_array($this->_handlers)) ? $this->_handlers : array();
  }

  /**
   * Schedules delete operations
   */
  protected function scheduleDeletes() {
  }

  /**
   * Schedules upsert operations
   */
  protected function scheduleUpserts() {
  }
}
