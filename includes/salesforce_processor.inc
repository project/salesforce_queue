<?php
class salesforce_processor {

  const MAX_SALESFORCE_CRON_PROCESS = 4000;
  const MAX_SALESFORCE_CRON_PROCESS_VARIABLE = 'salesforce_processor_max_salesforce_process';
  const MAX_SALESFORCE_REQUEST = 400;
  const MAX_SALESFORCE_REQUEST_VARIABLE = 'salesforce_processor_max_request_';
  const MAX_DRUPAL_CRON_PROCESS = 400;
  const MAX_DRUPAL_CRON_PROCESS_VARIABLE = 'salesforce_processor_max_drupal_process';

  /**
   * @var int
   */
  protected $_min_time = 0;

  protected $_max_salesforce_process = 0;

  protected $_salesforce_handlers = array();

  protected $_max_drupal_process = 0;

  /**
   * Default constructor
   */
  public function __construct() {
    $this->_min_time = strtotime('-29 days');
  }

  /**
   * Returns the maximum number of SalesForce items to process
   *
   * @return int
   */
  public function getMaxSalesforceProcess() {
    if (0 == $this->_max_salesforce_process) {
      $this->setMaxSalesforceProcess(variable_get(self::MAX_SALESFORCE_CRON_PROCESS_VARIABLE, self::MAX_SALESFORCE_CRON_PROCESS));
    }

    return $this->_max_salesforce_process;
  }

  /**
   * Sets the max number of SalesForce items to process
   *
   * @param int $max_process
   *
   * @return salesforce_processor
   */
  public function setMaxSalesforceProcess($max_process) {
    $this->_max_salesforce_process = (int)$max_process;
    return $this;
  }

  /**
   * Returns the maximum number of Drupal items to process
   *
   * @return int
   */
  public function getMaxDrupalProcess() {
    if (0 == $this->_max_salesforce_process) {
      $this->setMaxDrupalProcess(variable_get(self::MAX_DRUPAL_CRON_PROCESS_VARIABLE, self::MAX_DRUPAL_CRON_PROCESS));
    }

    return $this->_max_drupal_process;
  }

  /**
   * Sets the max number of Drupal items to process
   *
   * @param int $max_process
   *
   * @return salesforce_processor
   */
  public function setMaxDrupalProcess($max_process) {
    $this->_max_drupal_process = (int)$max_process;
    return $this;
  }

  /**
   * Sets the SalesForce handlers
   *
   * @param array|salesforce_handler $salesforce_handlers
   *
   * @return salesforce_processor
   */
  public function setSalesforceHandlers($salesforce_handlers) {
    if ($salesforce_handlers instanceof salesforce_handler) {
      $salesforce_handlers = array($salesforce_handlers->sfobj => array($salesforce_handlers));
    }

    if (TRUE === is_array($salesforce_handlers)) {
      $this->_salesforce_handlers = $salesforce_handlers;
    }

    return $this;
  }

  /**
   * Returns the SalesForce handlers
   *
   * @return array
   */
  public function getSalesforceHandlers() {
    return $this->_salesforce_handlers;
  }

  /**
   * Sets the minimum time
   *
   * @param int $time
   */
  public function setMinTime($time) {
    $time = (int)$time;

    // Salesforce allows getUpdated to be checked as far back as 30 days
    $min_time = strtotime('-29 days');
    $time = ($time >= $min_time) ? $time : $min_time;

    // There's a minimum time period of 60 seconds between start and end time
    if ((time() - $time) <= 60) {
      $time = time() - 61;
    }

    $this->_min_time = (int)$time;
    return $this;
  }

  /**
   * Gets the minimum time to search for
   *
   * @return int
   */
  public function getMinTime() {
    return $this->_min_time;
  }

  /**
   * Get a list of all deleted items from Salesforce
   *
   * @param string $sfobj
   *
   * @return array
   */
  protected function getDeletedRecords($sfobj) {
    // Attempt to connect to Salesforce.
    $sf = salesforce_api_connect();

    if (!$sf) {
      drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
      return FALSE;
    }

    $now = time();

    $request_successful = TRUE;
    $deleted_records = array();

    $records = $this->getQueuedRecords($sfobj, 'delete');

    $this->clearQueuedRecords($sfobj, $records, 'delete');

    return $records;
  }

  /**
   * Gets the list of all updated records from Salesforce
   *
   * @param string $sfobj
   *
   * @return array
   */
  protected function getUpdatedRecords($sfobj) {
    // Attempt to connect to Salesforce.
    $sf = salesforce_api_connect();
    if (!$sf) {
      drupal_set_message(t('Unable to connect to Salesforce using <a href="!url">current credentials</a>.', array('!url' => url(SALESFORCE_PATH_ADMIN))));
      return FALSE;
    }

    $now = time();

    $updates = array();

    $updated_items = $this->getQueuedRecords($sfobj, 'update');

    $results = array();

    if (0 < sizeof($updated_items)) {
      $objects = salesforce_api_fieldmap_objects_load('salesforce', $sfobj);

      $fields = implode(',', array_keys($objects['fields']));

      while (0 < sizeof($updated_items)) {
        // Allow per-object-type max values
        $max_request = variable_get(self::MAX_SALESFORCE_REQUEST_VARIABLE . $sfobj, self::MAX_SALESFORCE_REQUEST);
        $request = array_splice($updated_items, 0, $max_request);

        $request_result = array();

        try {
          $request_result = $sf->client->retrieve($fields, $sfobj, $request);
          // Make sure the result is an array
          $request_result = (TRUE === is_array($request_result)) ? $request_result : array($request_result);
        }
        catch (Exception $e) {
          salesforce_api_log(SALESFORCE_LOG_SOME, 'Exception while attempting to get updated records: ' . $e->getMessage(), array(), WATCHDOG_ERROR);
        }

        $results = array_merge($results, $request_result);
      }
    }

    return $results;
  }

  /**
   * Deletes queued items for a SalesForce Object Type, IDs, and operation
   *
   * @param string $sfobj
   * @param array $sfids
   * @param string $operation
   */
  protected function clearQueuedRecords($sfobj, $sfids, $operation) {
    if (FALSE === empty($sfids)) {
      // Generate placeholders for SFIDs
      $placeholders = implode(',', array_fill(0, count($sfids), "'%s'"));

      // Add operation and sfobj
      array_unshift($sfids, $operation);
      array_unshift($sfids, $sfobj);

      db_query("DELETE FROM {salesforce_queue_sf} WHERE sfobj = '%s' AND operation = '%s' AND sfid IN ({$placeholders})", $sfids);
    }
  }

  /**
   * Returns queued records for a SalesForce Object Type and operation
   *
   * @param string $sfobj
   * @param string $operation
   * @param integer $count
   *
   * @return array
   */
  protected function getQueuedRecords($sfobj, $operation) {
    $records = array();

    $result = db_query_range("SELECT * FROM {salesforce_queue_sf} WHERE sfobj = '%s' AND operation = '%s'", $sfobj, $operation, 0, $this->getMaxSalesforceProcess());

    while ($row = db_fetch_object($result)) {
      $records[] = $row->sfid;
    }

    return $records;
  }

  /**
   * Returns whether there are currently SalesForce items currently queued
   *
   * @return boolean
   */
  protected function hasSalesforceQueuedItems() {
    $count = db_result(db_query('SELECT COUNT(*) FROM {salesforce_queue_sf}'));

    return (0 < $count);
  }
}
