<?php
class salesforce_node_handler extends salesforce_handler {
  public function optionsForm() {
    $form = parent::optionsForm();

    $options = array();
    foreach (content_types() as $key => $content_type) {
      $options[$key] = $content_type['name'];
    }

    $form['node_type'] = array(
      '#type' => 'select',
      '#title' => t('Node Type'),
      '#options' => count($options) > 0 ? $options : array(t('None available')),
      '#disabled' => count($options) == 0,
      '#required' => FALSE,
      '#default_value' => $this->getNodeType(),
    );

    return $form;
  }

  /**
   * Return whether this handler can work with the particular node type
   *
   * @return boolean
   */
  public function isHandlerFor($object) {
    return (TRUE === isset($object->type) && $this->getDrupalType() == $object->type);
  }

  public function setNodeType($node_type) {
    $this->_settings['node_type'] = $node_type;
    return $this;
  }

  public function getNodeType() {
    if (TRUE === isset($this->_settings['node_type'])) {
      return $this->_settings['node_type'];
    }

    return '';
  }

  public function getDrupalType() {
    return $this->getNodeType();
  }

  protected function getRecordTypes() {
    if (NULL == $this->_record_types) {
      $this->_record_types = score_salesforce_get_record_types();
    }

    return $this->_record_types;
  }
}