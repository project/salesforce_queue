<?php
class salesforce_node_scheduler extends salesforce_scheduler {
  /**
   * Schedule items
   *
   * @param boolean $force
   * Whether the scheduler should schedule all items
   *
   * @return salesforce_scheduler
   */
  public function schedule($force = FALSE) {
    if (TRUE === $force) {
      $this->scheduleDeletes();
      $this->scheduleUpserts();
    }

    return $this;
  }

  /**
   * Schedule nodes for upserts
   */
  protected function scheduleUpserts() {
    $args = array();

    $handlers = $this->getHandlers();

    foreach ($handlers as $handler) {
      if ('upsert' == $handler->getOperation()) {
        if ($handler instanceof salesforce_node_handler) {
          $args[] = $handler->getNodeType();
        }
      }
    }

    if (0 < sizeof($args)) {
      $placeholders = implode(',', array_fill(0, count($args), "'%s'"));

      $args[] = $this->getLastRun();

      $result = db_query("SELECT nid, type FROM {node} WHERE status = 1 AND type IN ($placeholders) AND changed > %d ORDER BY nid DESC", $args);

      while ($node_info = db_fetch_object($result)) {
        $node = node_load($node_info->nid);

        foreach (salesforce_queue_get_drupal_object_handler('upsert', $node) as $handler) {
          if ($handler instanceof salesforce_handler_upsert) {
            $handler->scheduleDrupalUpsert($node);
          }
        }
      }
    }
  }

  /**
   * Schedule nodes for deletion
   */
  protected function scheduleDeletes() {
    $node_types = array();

    foreach ($this->getHandlers() as $handler) {
      if ('delete' == $handler->getOperation()) {
        if ($handler instanceof salesforce_node_handler) {
          $node_types[] = $handler->getNodeType();
        }
      }
    }

    if (0 < sizeof($node_types)) {
      $placeholders = implode(',', array_fill(0, count($node_types), "'%s'"));

      $result = db_query("SELECT n.nid, n.vid, som.drupal_type AS type, som.* FROM {salesforce_object_map} som
      	LEFT OUTER JOIN {node} n ON n.nid = som.oid
      	WHERE n.nid IS NULL AND som.drupal_type IN ($placeholders)", $node_types);

      while ($delete_info = db_fetch_object($result)) {
        foreach (salesforce_queue_get_drupal_object_handler('delete', $delete_info) as $handler) {
          if ($handler instanceof salesforce_handler_delete) {
            $handler->scheduleDrupalDelete($delete_info);
          }
        }
      }
    }
  }
}
